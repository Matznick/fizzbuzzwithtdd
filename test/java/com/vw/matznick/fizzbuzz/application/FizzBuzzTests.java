package com.vw.matznick.fizzbuzz.application;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTests {

    @Test
    void fizzBuzzTest(){
        final FizzBuzz fb = new FizzBuzz();
    assertEquals("Fizz",fb.fizzBuzz(3));
    assertEquals("Buzz",fb.fizzBuzz(5));
    assertEquals("FizzBuzz",fb.fizzBuzz(15));
    assertEquals("Fizz",fb.fizzBuzz(3));
    assertEquals("4",fb.fizzBuzz(4));
    assertEquals("0",fb.fizzBuzz(0));
    assertEquals("-1",fb.fizzBuzz(-1));
    assertEquals("Fizz",fb.fizzBuzz(-3));
    assertEquals("Buzz",fb.fizzBuzz(-5));
    assertEquals("FizzBuzz",fb.fizzBuzz(-30));
    }
}
