package com.vw.matznick.fizzbuzz.application;

public class FizzBuzz {

    String fizzBuzz(int numberToCheck){
        String result = "";
        if (numberToCheck == 0){
            return "0";
        }
        if (numberToCheck % 3 == 0 && numberToCheck % 5 == 0){
            return "FizzBuzz";
        }
        if (numberToCheck % 3 == 0){
            return "Fizz";
        }
        if (numberToCheck % 5 == 0){
            return "Buzz";
        }
        return String.valueOf(numberToCheck);
    }


}
